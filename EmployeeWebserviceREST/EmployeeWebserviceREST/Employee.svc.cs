﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeWebserviceREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Employee" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Employee.svc or Employee.svc.cs at the Solution Explorer and start debugging.
    public class Employee : IEmployee
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();


       
        public bool AddEmployee(Employee eml)
        {
            throw new NotImplementedException();
        }
        public bool DeleteEmployee(int idE)
        {
            throw new NotImplementedException();
        }
        public bool UpdateEmployee(Employee eml)
        {
            throw new NotImplementedException();
        }
        List<Employee> IEmployee.GetProductList()
        {
            throw new NotImplementedException();
        }

        public bool AddEmployee(Employee1 eml)
        {
            try
            {
                data.Employee1s.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployee1(int idE)
        {
            try
            {
                Employee1 employeeToDelete =
                    (from employee in data.Employee1s where employee.employeeID == idE select employee).Single();
                data.Employee1s.DeleteOnSubmit(employeeToDelete);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<Employee1> GetProductList()
        {
            try
            {
                return (from employee in data.Employee1s select employee).ToList();
            }
            catch
            {
                return null;
            }
        }

        
        public bool UpdateEmployee1(Employee eml)
        {
            Employee1 employeeToModify =
                (from employee in data.Employee1s where employee.employeeID == eml.employeeID select employee).Single();
            employeeToModify.Age = eml.Age;
            employeeToModify.address = eml.address;
            employeeToModify.firstName = eml.firstName;
            employeeToModify.lastName = eml.lastName;
            data.SubmitChanges();
            return true;
        }

       
    }
