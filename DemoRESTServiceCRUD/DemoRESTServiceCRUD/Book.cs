﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DemoRESTServiceCRUD
{
    [DataContract]
    public class Book
    {
        [DataMember]

        public int BookId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ISBN { get; set; }


    }
    public interface IBookRepository
    {
        List<Book> GetAllBooks();

        Book GetBookById(int id);
        Book AddNewBook(Book item);
        bool DeleteABook(int id);
         bool  UpdateABook(Book item);
    }
    public class BookRepository : IBookRepository
    {
        private List<Book> books = new List<Book>();
        private int counter = 1;
        public BookRepository()
        {
            AddNewBook(new Book { Title = "C# Programing", ISBN = "23422342343" });
            AddNewBook(new Book { Title = "Java Programing", ISBN = "123123123543" });
            AddNewBook(new Book { Title = "WCF Programing", ISBN = "231324234" });
        }

        public Book AddNewBook(Book newBook)
        {
            if (newBook == null)
                throw new ArgumentNullException("newBook");
            newBook.BookId = counter++;
            books.Add(newBook);
            return newBook;

        }

        public List<Book> GetAllBooks()
        {
            return books;
        }
        public Book GetBookById (int bookId)
        {
            return books.Find(b => b.BookId == bookId);
        }

        public bool DeleteABook(int bookId)
        {
            int idx = books.FindIndex(b => b.BookId == bookId);
            if (idx == -1)
                return false;
            books.RemoveAll(b => b.BookId == bookId);
            return true;

        }

        public bool UpdateABook(Book updateBook)
        {
            if (updateBook == null) ;
            throw new ArgumentNullException("updateBook");

            int idx = books.FindIndex(b => b.BookId == updateBook.BookId);
            if (idx == -1)
                return false;

            books.RemoveAt(idx);
            books.Add(updateBook);
            return true;
        }
    }
}