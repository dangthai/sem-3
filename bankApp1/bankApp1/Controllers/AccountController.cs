﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bankApp1.Models;
using System.Data.Entity;
using System.Net;

namespace bankApp1.Controllers
{
    public class AccountController : Controller
    {
        bankAppContext db = new bankAppContext();
        // GET: Account
        public ActionResult Index()
        {
            var account = db.ACCOUNT.Include(a => a.BRANCH).Include(a => a.CUSTOMER).Include(a => a.EMPLOYEE).Include(a => a.PRODUCT);
            return View(account.ToList());

        }

        // GET: Account/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ACCOUNT aCCOUNT = db.ACCOUNT.Find(id);
            if (aCCOUNT == null)
            {
                return HttpNotFound();
            }
            return View(aCCOUNT);

        }

        // GET: Account/Create
        public ActionResult Create()
        {
            ViewBag.OPEN_BRANCH_ID = new SelectList(db.BRANCH, "BRANCH_ID", "ADDRESS");
            ViewBag.CUST_ID = new SelectList(db.CUSTOMER, "CUST_ID", "ADDRESS");
            ViewBag.OPEN_EMP_ID = new SelectList(db.EMPLOYEE, "EMP_ID", "FIRST_NAME");
            ViewBag.PRODUCT_CD = new SelectList(db.PRODUCT, "PRODUCT_CD", "NAME");
            return View();

        }

        // POST: Account/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ACCOUNT_ID,AVAIL_BALANCE,CLOSE_DATE,LAST_ACTIVITY_DATE,OPEN_DATE,PENDING_BALANCE,STATUS,CUST_ID,OPEN_BRANCH_ID,OPEN_EMP_ID,PRODUCT_CD")] ACCOUNT ACCOUNT)
        {
            if (ModelState.IsValid)
            {
                db.ACCOUNT.Add(ACCOUNT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OPEN_BRANCH_ID = new SelectList(db.BRANCH, "BRANCH_ID", "ADDRESS", ACCOUNT.OPEN_BRANCH_ID);
            ViewBag.CUST_ID = new SelectList(db.CUSTOMER, "CUST_ID", "ADDRESS", ACCOUNT.CUST_ID);
            ViewBag.OPEN_EMP_ID = new SelectList(db.EMPLOYEE, "EMP_ID", "FIRST_NAME", ACCOUNT.OPEN_EMP_ID);
            ViewBag.PRODUCT_CD = new SelectList(db.PRODUCT, "PRODUCT_CD", "NAME", ACCOUNT.PRODUCT_CD);
            return View(ACCOUNT);

        }

        // GET: Account/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ACCOUNT ACCOUNT = db.ACCOUNT.Find(id);
            if (ACCOUNT == null)
            {
                return HttpNotFound();
            }
            ViewBag.OPEN_BRANCH_ID = new SelectList(db.BRANCH, "BRANCH_ID", "ADDRESS", ACCOUNT.OPEN_BRANCH_ID);
            ViewBag.CUST_ID = new SelectList(db.CUSTOMER, "CUST_ID", "ADDRESS", ACCOUNT.CUST_ID);
            ViewBag.OPEN_EMP_ID = new SelectList(db.EMPLOYEE, "EMP_ID", "FIRST_NAME", ACCOUNT.OPEN_EMP_ID);
            ViewBag.PRODUCT_CD = new SelectList(db.PRODUCT, "PRODUCT_CD", "NAME", ACCOUNT.PRODUCT_CD);
            return View(ACCOUNT);

        }

        // POST: Account/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ACCOUNT_ID,AVAIL_BALANCE,CLOSE_DATE,LAST_ACTIVITY_DATE,OPEN_DATE,PENDING_BALANCE,STATUS,CUST_ID,OPEN_BRANCH_ID,OPEN_EMP_ID,PRODUCT_CD")] ACCOUNT ACCOUNT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ACCOUNT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OPEN_BRANCH_ID = new SelectList(db.ACCOUNT, "BRANCH_ID", "ADDRESS", ACCOUNT.OPEN_BRANCH_ID);
            ViewBag.CUST_ID = new SelectList(db.CUSTOMER, "CUST_ID", "ADDRESS", ACCOUNT.CUST_ID);
            ViewBag.OPEN_EMP_ID = new SelectList(db.EMPLOYEE, "EMP_ID", "FIRST_NAME", ACCOUNT.OPEN_EMP_ID);
            ViewBag.PRODUCT_CD = new SelectList(db.PRODUCT, "PRODUCT_CD", "NAME", ACCOUNT.PRODUCT_CD);
            return View(ACCOUNT);

        }

        // GET: Account/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ACCOUNT aCCOUNT = db.ACCOUNT.Find(id);
            if (aCCOUNT == null)
            {
                return HttpNotFound();
            }
            return View(aCCOUNT);

        }

        // POST: Account/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection collection)
        {
            ACCOUNT ACCOUNT = db.ACCOUNT.Find(id);
            db.ACCOUNT.Remove(ACCOUNT);
            db.SaveChanges();
            return RedirectToAction("Index");

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
