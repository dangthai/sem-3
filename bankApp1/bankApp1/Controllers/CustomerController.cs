﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bankApp1.Models;
using System.Data.Entity;
using System.Net;

namespace bankApp1.Controllers
{
    public class CustomerController : Controller
    {
        bankAppContext db = new bankAppContext();
        // GET: Customer
        public ActionResult Index()
        {
            var customer = db.CUSTOMER.Include(data => data.BUSINESS).Include(data=>data.INDIVIDUAL);
            return View(customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMER.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);

        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            ViewBag.CUST_ID = new SelectList(db.BUSINESS, "CUST_ID", "NAME");
            ViewBag.CUST_ID = new SelectList(db.INDIVIDUAL, "CUST_ID", "FIRST_NAME");
            return View();

        }

        // POST: Customer/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CUST_ID,ADDRESS,CITY,CUST_TYPE_CD,FED_ID,POSTAL_CODE,STATE")] CUSTOMER CUSTOMER)
        {
            if (ModelState.IsValid)
            {
                db.CUSTOMER.Add(CUSTOMER);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CUST_ID = new SelectList(db.BUSINESS, "CUST_ID", "NAME", CUSTOMER.CUST_ID);
            ViewBag.CUST_ID = new SelectList(db.INDIVIDUAL, "CUST_ID", "FIRST_NAME", CUSTOMER.CUST_ID);
            return View(CUSTOMER);

        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER CUSTOMER = db.CUSTOMER.Find(id);
            if (CUSTOMER == null)
            {
                return HttpNotFound();
            }
            ViewBag.CUST_ID = new SelectList(db.BUSINESS, "CUST_ID", "NAME", CUSTOMER.CUST_ID);
            ViewBag.CUST_ID = new SelectList(db.INDIVIDUAL, "CUST_ID", "FIRST_NAME", CUSTOMER.CUST_ID);
            return View(CUSTOMER);

        }

        // POST: Customer/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CUST_ID,ADDRESS,CITY,CUST_TYPE_CD,FED_ID,POSTAL_CODE,STATE")] CUSTOMER CUSTOMER)
        {
            if (ModelState.IsValid)
            {
                db.Entry(CUSTOMER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CUST_ID = new SelectList(db.BUSINESS, "CUST_ID", "NAME", CUSTOMER.CUST_ID);
            ViewBag.CUST_ID = new SelectList(db.INDIVIDUAL, "CUST_ID", "FIRST_NAME", CUSTOMER.CUST_ID);
            return View(CUSTOMER);

        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMER.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);

        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            CUSTOMER cUSTOMER = db.CUSTOMER.Find(id);
            db.CUSTOMER.Remove(cUSTOMER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
