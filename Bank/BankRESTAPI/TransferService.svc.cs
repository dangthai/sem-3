﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BankRESTAPI
{
    public class TransferService : ITransferService
    {
        TransferAction transferAction = new TransferAction();

        public bool NewTransfer(NewTransfer newTransfer)
        {
            return transferAction.AddNewTransfer(newTransfer);
        }

        public List<ClientGetTransferHistoryResult> ClientGetTransferHistory(ClientGetTransferHistory data)
        {
            return transferAction.ClientGetTransferHistory(data);
        }

        public List<PartnerGetTransferHistoryResult> PartnerGetTransferHistory(PartnerGetTransferHistory data)
        {
            return transferAction.PartnerGetTransferHistory(data);
        }
    }
}
