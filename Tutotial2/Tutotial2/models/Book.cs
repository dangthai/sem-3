﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutotial2.models
{
    public class Book
    {
        public int BookId { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string CoverImage { get; set; }
    }
    public class BookManager
    {
        public static List<Book> GetBooks()
        {
            var books = new List<Book>();

            books.Add(new Book { BookId = 1, Title = "Vulpate", Author = "Futurum", CoverImage = "Assets/books/1.png" });
            books.Add(new Book { BookId = 2, Title = "Mazim", Author = "Sepuiter Que", CoverImage = "Assets/books/2.png" });
            books.Add(new Book { BookId = 3, Title = "Elit", Author = "Tempor", CoverImage = "Assets/books/3.png" });
            books.Add(new Book { BookId = 4, Title = "Etiam", Author = "Option", CoverImage = "Assets/books/4.png" });
            books.Add(new Book { BookId = 5, Title = "Feugait Eros Libox", Author = "Accumsan", CoverImage = "Assets/books/5.png" });
            books.Add(new Book { BookId = 6, Title = "Nonunmy", Author = "Legunt Xaeplus", CoverImage = "Assets/books/6.png" });
            books.Add(new Book { BookId = 7, Title = "Nostrud", Author = "Eleifend", CoverImage = "Assets/books/7.png" });
            books.Add(new Book { BookId = 8, Title = "Per Modo", Author = "Vero Tation", CoverImage = "Assets/books/8.png" });
            books.Add(new Book { BookId = 9, Title = "Suscipit", Author = "Jack Tibbles", CoverImage = "Assets/books/9.png" });
            books.Add(new Book { BookId = 10, Title = "Decima", Author = "Iuffy Tibbles", CoverImage = "Assets/books/10.png" });
            books.Add(new Book { BookId = 11, Title = "Erat", Author = "Volupat", CoverImage = "Assets/books/11.png" });
            books.Add(new Book { BookId = 12, Title = "Consequat", Author = "Est Possim", CoverImage = "Assets/books/12.png" });
            books.Add(new Book { BookId = 13, Title = "Aliquip", Author = "Magna", CoverImage = "Assets/books/13.png" });

            return books;
        }
    }
}


