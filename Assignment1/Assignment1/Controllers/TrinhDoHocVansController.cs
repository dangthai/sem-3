﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment1.Models;

namespace Assignment1.Controllers
{
    public class TrinhDoHocVansController : Controller
    {
        private NhanVienContext db = new NhanVienContext();

        // GET: TrinhDoHocVans
        public ActionResult Index()
        {
            return View(db.TrinhDoHocVans.ToList());
        }

        // GET: TrinhDoHocVans/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrinhDoHocVan trinhDoHocVan = db.TrinhDoHocVans.Find(id);
            if (trinhDoHocVan == null)
            {
                return HttpNotFound();
            }
            return View(trinhDoHocVan);
        }

        // GET: TrinhDoHocVans/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TrinhDoHocVans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaTDHV,TenTrinhDo,ChuyenNganh")] TrinhDoHocVan trinhDoHocVan)
        {
            if (ModelState.IsValid)
            {
                db.TrinhDoHocVans.Add(trinhDoHocVan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(trinhDoHocVan);
        }

        // GET: TrinhDoHocVans/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrinhDoHocVan trinhDoHocVan = db.TrinhDoHocVans.Find(id);
            if (trinhDoHocVan == null)
            {
                return HttpNotFound();
            }
            return View(trinhDoHocVan);
        }

        // POST: TrinhDoHocVans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaTDHV,TenTrinhDo,ChuyenNganh")] TrinhDoHocVan trinhDoHocVan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trinhDoHocVan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(trinhDoHocVan);
        }

        // GET: TrinhDoHocVans/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrinhDoHocVan trinhDoHocVan = db.TrinhDoHocVans.Find(id);
            if (trinhDoHocVan == null)
            {
                return HttpNotFound();
            }
            return View(trinhDoHocVan);
        }

        // POST: TrinhDoHocVans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TrinhDoHocVan trinhDoHocVan = db.TrinhDoHocVans.Find(id);
            db.TrinhDoHocVans.Remove(trinhDoHocVan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
