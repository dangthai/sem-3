﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment1.Models;

namespace Assignment1.Controllers
{
    public class ThoiGianCongTacsController : Controller
    {
        private NhanVienContext db = new NhanVienContext();

        // GET: ThoiGianCongTacs
        public ActionResult Index()
        {
            var thoiGianCongTacs = db.ThoiGianCongTacs.Include(t => t.ChucVu).Include(t => t.NhanVien);
            return View(thoiGianCongTacs.ToList());
        }

        // GET: ThoiGianCongTacs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThoiGianCongTac thoiGianCongTac = db.ThoiGianCongTacs.Find(id);
            if (thoiGianCongTac == null)
            {
                return HttpNotFound();
            }
            return View(thoiGianCongTac);
        }

        // GET: ThoiGianCongTacs/Create
        public ActionResult Create()
        {
            ViewBag.MaCV = new SelectList(db.ChucVus, "MaCV", "TenCV");
            ViewBag.MaNV = new SelectList(db.NhanViens, "MaNV", "HoTen");
            return View();
        }

        // POST: ThoiGianCongTacs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaNV,MaCV,NgayNhanChuc")] ThoiGianCongTac thoiGianCongTac)
        {
            if (ModelState.IsValid)
            {
                db.ThoiGianCongTacs.Add(thoiGianCongTac);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MaCV = new SelectList(db.ChucVus, "MaCV", "TenCV", thoiGianCongTac.MaCV);
            ViewBag.MaNV = new SelectList(db.NhanViens, "MaNV", "HoTen", thoiGianCongTac.MaNV);
            return View(thoiGianCongTac);
        }

        // GET: ThoiGianCongTacs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThoiGianCongTac thoiGianCongTac = db.ThoiGianCongTacs.Find(id);
            if (thoiGianCongTac == null)
            {
                return HttpNotFound();
            }
            ViewBag.MaCV = new SelectList(db.ChucVus, "MaCV", "TenCV", thoiGianCongTac.MaCV);
            ViewBag.MaNV = new SelectList(db.NhanViens, "MaNV", "HoTen", thoiGianCongTac.MaNV);
            return View(thoiGianCongTac);
        }

        // POST: ThoiGianCongTacs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaNV,MaCV,NgayNhanChuc")] ThoiGianCongTac thoiGianCongTac)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thoiGianCongTac).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MaCV = new SelectList(db.ChucVus, "MaCV", "TenCV", thoiGianCongTac.MaCV);
            ViewBag.MaNV = new SelectList(db.NhanViens, "MaNV", "HoTen", thoiGianCongTac.MaNV);
            return View(thoiGianCongTac);
        }

        // GET: ThoiGianCongTacs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThoiGianCongTac thoiGianCongTac = db.ThoiGianCongTacs.Find(id);
            if (thoiGianCongTac == null)
            {
                return HttpNotFound();
            }
            return View(thoiGianCongTac);
        }

        // POST: ThoiGianCongTacs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ThoiGianCongTac thoiGianCongTac = db.ThoiGianCongTacs.Find(id);
            db.ThoiGianCongTacs.Remove(thoiGianCongTac);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
